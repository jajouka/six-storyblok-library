const path = require('path');
module.exports = {
  module: {
    rules: [
      {
        test: /\.s?css$/,
        loaders: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
        include: path.resolve(__dirname, '../')
      },
      {
        test: /\.styl$/,
        use: [
          {
              loader: 'style-loader' // creates style nodes from JS strings
          },
          {
              loader: 'css-loader' // translates CSS into CommonJS
          },
          {
            loader: 'stylus-loader' // compiles Stylus to CSS
          }
        ]
      }
    ]
  },
  resolve: {

  }
};
