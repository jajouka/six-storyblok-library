import Vue from 'vue'
import Vuex from 'vuex'
import 'storybook-chromatic';

import { configure, addDecorator } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

// Styles
import '@/src/assets/scss/_storybook.scss'

import { addParameters } from '@storybook/vue';
import { themes } from '@storybook/theming';
import sixTheme from './sixTheme';

// Option defaults.
addParameters({
  options: {
    theme: sixTheme,
  },
});


function loadStories() {
  const req = require.context('../src/components', true, /\.stories\.js$/)
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module);





addDecorator(() => ({
  template: '<v-app><story/></v-app>',
}));


import AtomImage from '../src/components/Atoms/Image/Image.vue';
import AtomText from '../src/components/Atoms/Text/Text.vue';
//import AtomTextarea from '../src/components/storyblok/Atoms/Textarea/Textarea.vue';
import AtomLink from '../src/components/Atoms/Link/Link.vue';
import AtomMarkdown from '../src/components/Atoms/Markdown/Markdown.vue';
import AtomVideoPlayer from '../src/components/Atoms/VideoPlayer/VideoPlayer.vue';

Vue.component('sbook-image', AtomImage);
Vue.component('sbook-atom-markdown', AtomMarkdown);
Vue.component('sbook-text', AtomText);
//Vue.component('sbook-textarea', AtomTextarea);
Vue.component('sbook-link', AtomLink);
Vue.component('sbook-markdown', AtomMarkdown);
Vue.component('sbook-videoplayer', AtomVideoPlayer);
