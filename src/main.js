import Vue from 'vue';
import App from './App.vue';
import Vuex from 'vuex';
//import router from './router/index.js';

import MyPlugin from './plugins/layouts.js';
Vue.use(MyPlugin);

Vue.config.productionTip = false;
Vue.use(Vuex);

new Vue({
  data(){
    return {
      message: this.$myAddedProperty
    };
  },
  mounted(){
    //console.log('this.message:');
    //console.log(this.message);
  },
  render: h => h(App)
}).$mount('#app');
