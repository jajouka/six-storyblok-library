// routes.js
import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);


import home from '../components/Home.vue';


const routes = [
  {
    path: '/',
    name: 'home',
    component: home
  },

];


export default new VueRouter({routes});
