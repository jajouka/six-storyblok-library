export default {
  methods: {


    getColModSbkCtrlOpts(sbkControls){
      const filtered = Object.entries(sbkControls).filter(
        ([key, val]) => typeof(val['type']) !== 'undefined' && val['type'] === 'col_mod'
      );
      return filtered;
    },

    buildSbkCntCtrlOpts(blok) {
      let sbkOptNames = [];
      let builtSbkCntCtrlOpts = {};
      let wrapperSbkOptDefs = this.getActiveWrapperSbkCtrlOpts();
      sbkOptNames = Object.keys(wrapperSbkOptDefs);
      for (let i = 0; i < sbkOptNames.length; i++) {
        builtSbkCntCtrlOpts[sbkOptNames[i]] = blok[sbkOptNames[i]];
      }
      return builtSbkCntCtrlOpts;
    },

    /*buildSbkCntCtrlOptsForCol(colBlok) {

    },*/

    //@TODO - KEEP THIS - check if cb function exists then use, if not use blok val
    /*buildSbkCntCtrlOpts(ctrlOpts){
    let funcVal = null;
    let cbParams = null;
    let builtSbkCntCtrlOpts = {};
    if(typeof (ctrlOpts) !== 'undefined'){
      if (Object.keys(ctrlOpts).length > 0){
        for (let [ctrlOptKey, ctrlOptValue] of Object.entries(ctrlOpts)){
          if(! Array.isArray(ctrlOptValue) && typeof (ctrlOptValue) === 'object'){
            if(Object.keys(ctrlOptValue).length > 0){
              let optProfile = this.getOptProfileByName(ctrlOptKey);
              if(Object.prototype.hasOwnProperty.call(optProfile, 'callback')){
                cbParams = {...ctrlOptValue.params, ...{default: ctrlOptValue.default}};
                funcVal = optProfile.callback(cbParams);
                builtSbkCntCtrlOpts[ctrlOptKey] = funcVal ? funcVal : ctrlOptValue.default;
              }
            }
          }
        }
        return builtSbkCntCtrlOpts;
      }
    }
    },*/




    getSbkCtrlOptsForCnt(colName, cntName, cntIdx){

      let sbkCtrlOpts = {};
      if (Object.prototype.hasOwnProperty.call(this, '_data')){
        if (Object.keys(this._data).length > 0){
          if (Object.prototype.hasOwnProperty.call(this._data, 'overrides')){
            if (Object.keys(this._data.overrides).length > 0){
              if (Object.prototype.hasOwnProperty.call(this._data.overrides, 'options')) {
                if (Object.keys(this._data.overrides.options).length > 0) {
                  if (Object.prototype.hasOwnProperty.call(this._data.overrides['options'], colName)) {
                    if (!Array.isArray(this._data.overrides['options'][colName]) && typeof (this._data.overrides['options'][colName]) === 'object') {
                      if (Object.keys(this._data.overrides['options'][colName]).length > 0) {
                        if (Object.prototype.hasOwnProperty.call(this._data.overrides['options'][colName], 'components')) {
                          if (!Array.isArray(this._data.overrides['options'][colName]['components']) && typeof (this._data.overrides['options'][colName]['components']) === 'object') {
                            if (Object.keys(this._data.overrides['options'][colName]['components']).length > 0) {
                              let cnts = Object.entries(this._data.overrides['options'][colName]['components']);
                              if (Object.prototype.hasOwnProperty.call(cnts[cntIdx][1], 'sbk_controls')) {
                                if (!Array.isArray(cnts[cntIdx][1]['sbk_controls']) && typeof (cnts[cntIdx][1]['sbk_controls']) === 'object') {
                                  if (Object.keys(cnts[cntIdx][1]['sbk_controls']).length > 0) {

                                    //sbkCtrlOpts = cnts[cntIdx][1]['sbk_controls'];

                                    for (let [ctrlName, ctrlValue] of Object.entries(cnts[cntIdx][1]['sbk_controls'])) {
                                      let sbkCtrlOpt = this.getCtrlValueFromBlokWithName(ctrlName);
                                      sbkCtrlOpts = {...sbkCtrlOpts, ...{[`${ctrlName}`]: sbkCtrlOpt}};
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return sbkCtrlOpts;
    },

    getCtrlValueFromBlokWithName(name){
      if(Object.prototype.hasOwnProperty.call(this.blok, name)){
        return this.blok[name];
      }
      return null;
    },

    getActiveWrapperSbkCtrlOpts(){
      let sbkCtrlOpts = {};
      if (Object.prototype.hasOwnProperty.call(this, '_data')){
        if (Object.keys(this._data).length > 0){
          if (Object.prototype.hasOwnProperty.call(this._data, 'overrides')){
            if (Object.keys(this._data.overrides).length > 0){
              if (Object.prototype.hasOwnProperty.call(this._data.overrides, 'options')){
                if (Object.keys(this._data.overrides.options).length > 0){
                  if (Object.prototype.hasOwnProperty.call(this._data.overrides.options, 'sbk_controls')){
                    if (Object.keys(this._data.overrides.options.sbk_controls).length > 0){
                      sbkCtrlOpts = this._data.overrides.options.sbk_controls;
                    }
                  }
                }
              }
            }
          }
        }
      }
      return sbkCtrlOpts;
    },

  }
};
