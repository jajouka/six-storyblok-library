export default {
  methods: {
    getEleMods(cnt) {

      let data = [];

      switch(cnt.component){
      case 'figure':
        data = this.figureMod();
        break;
      case 'test':
        data = this.testMod();
        break;
      //default : [];
      }

      return data;


    },

    figureMod(){

    },

    testMod(){

    }
  }
};
