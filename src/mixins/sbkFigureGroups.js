export default {

  methods: {
    hasFigures(){
      return this.figures().length > 0;
    },
    isFigure(num){
      let figures = this.figures();
      if(!figures[num] ) {
        return false;
      }
      if( Object.keys(figures[num]).length > 0){
        if(Object.prototype.hasOwnProperty.call(figures[num], 'component')){
          if(figures[num]['component'] === 'figure'){
            return true;
          }
        }
      }
      return false;
    },
    figures(){
      let figures = [];
      if(Object.prototype.hasOwnProperty.call(this.blok, 'figures')) {
        if (typeof (this.blok['figures']) !== 'undefined') {
          if (Array.isArray(this.blok['figures']) && typeof (this.blok['figures']) === 'object') {
            if (this.blok['figures'].length > 0) {
              figures = this.blok['figures'];
            }
          }
        }
      }
      else if(Object.prototype.hasOwnProperty.call(this.blok, 'items')) {
        if (typeof (this.blok['items']) !== 'undefined') {
          if (Array.isArray(this.blok['items']) && typeof (this.blok['items']) === 'object') {
            if (this.blok['items'].length > 0) {
              figures = this.blok['items'];
            }
          }
        }
      }
      return figures;
    },
    imageBlok(itemNum){

      //@TODO - move this out of layout for common usage
      //@TODO - good example for type-safe common checker function, to replace lots and lots of code!

      let blok = {};

      if(Object.prototype.hasOwnProperty.call(this.blok, 'items')){
        if(typeof(this.blok['items']) !== 'undefined'){
          if(Array.isArray(this.blok['items']) && typeof (this.blok['items']) === 'object'){
            if(this.blok['items'].length > 0){
              if(this.blok['items'][itemNum] !== void 0){
                //@TODO - needs proper checks here
                let images = [];
                images.push(this.blok['items'][itemNum]['images'][0]);
                blok['images'] = images;
                let captions = [];
                captions.push(this.blok['items'][itemNum]['captions'][0]);
                blok['captions'] = captions;
              }
            }
          }
        }
      }
      if(Object.prototype.hasOwnProperty.call(this.blok, 'figures')){
        if(typeof(this.blok['figures']) !== 'undefined'){
          if(Array.isArray(this.blok['figures']) && typeof (this.blok['figures']) === 'object'){
            if(this.blok['figures'].length > 0){
              if(this.blok['figures'][itemNum] !== void 0){
                //@TODO - needs proper checks here
                let images = [];
                images.push(this.blok['figures'][itemNum]['images'][0]);
                blok['images'] = images;
                let captions = [];
                captions.push(this.blok['figures'][itemNum]['captions'][0]);
                blok['captions'] = captions;
              }
            }
          }
        }
      }

      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_classes')){
        if(typeof(this.blok['image_classes']) === 'string'){
          if(this.blok['image_classes'].trim() !== ''){
            blok['image_classes'] = this.blok['image_classes'];
          }
        }
      }

      //@TODO - needs proper checks

      blok['image_size'] = this.imageSizes[itemNum];

      //@TODO - not using these 2 at the moment, need to blend this with colBlok code, needs wrapper control

      /*if(Object.prototype.hasOwnProperty.call(this.blok, 'position_wrap_classes')){
        if(typeof(this.blok['position_wrap_classes']) === 'string'){
          if(this.blok['position_wrap_classes'].trim() !== ''){
            blok['position_wrap_classes'] = this.blok['position_wrap_classes'];
          }
        }
      }*/

      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_col_sizes')){
        if(!Array.isArray(this.blok['image_col_sizes']) && typeof(this.blok['image_col_sizes']) === 'object'){
          if(Object.keys(this.blok['image_col_sizes']).length > 0){
            blok['image_col_sizes'] = this.blok['image_col_sizes'];
          }
        }
      }

      return blok;
    }
  },

};
