//@TODO - only register opt profile if a function is needed to calculate a value? no, they need registering
//@TODO - not used yet - pattern needs more thinking - maybe be only used for overriding
export default {
  data() {
    let self = this;
    return {
      sbkCtrlOptProfiles: {
        image_size: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_ImageSize(params);
          }
        },
        position_h: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_PositionH(params);
          }
        },
        position_v: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_PositionV(params);
          }
        },
        text_align: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_TextAlign(params);
          }
        },
        reverse: {
          type: 'boolean',
          callback: function (params) {
            return self.sbkCtrl_Reverse(params);
          }
        },
        caption_position: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_CaptionPosition(params);
          }
        },
        show_captions: {
          type: 'boolean',
          callback: function (params) {
            return self.sbkCtrl_ShowCaptions(params);
          }
        },
        title: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_Title(params);
          }
        },
        overlay: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_Overlay(params);
          }
        },
        left_gutter: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_LeftGutter(params);
          }
        },
        right_gutter: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_RightGutter(params);
          }
        },
        remove_top_space: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_RemoveTopSpace(params);
          }
        },
        remove_bottom_space: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_RemoveBottomSpace(params);
          }
        },

        block_style: {
          type: 'string',
          callback: function (params) {
            return self.sbkCtrl_RemoveBottomSpace(params);
          }
        },

      }
    };
  },

  methods: {
    //@TODO - assumes width is always present
    //@TODO - assumes width is the base factor for ratio calc

    getSbkCtrlKeys(){
      let keys = [];
      if(Object.prototype.hasOwnProperty.call(this._data, 'sbkCtrlOptProfiles')){
        if(Object.keys(this._data.sbkCtrlOptProfiles).length > 0){
          return Object.keys(this._data.sbkCtrlOptProfiles);
        }
      }
    },
    sbkCtrl_ImageSize(params){
      console.log('sbkCtrl_ImageSize!!!!');
      console.log('params:');
      console.log(params);

      //@TODO - which default to use?

      let selImgSize = null;
      let calculatedSize = null;
      let ratioNums = null;
      let aspectRatio = null;

      if(Object.prototype.hasOwnProperty.call(params, 'default')){
        if(typeof (params.default) === 'string'){
          if(params.default !== ''){
            if(Object.prototype.hasOwnProperty.call(params, 'image_sizes')){
              if(! Array.isArray(params.image_sizes) && typeof(params.image_sizes) === 'object'){
                if(Object.keys(params.image_sizes).find(key => key === params.default)){
                  selImgSize = params.image_sizes[params.default];
                  if(! Array.isArray(selImgSize) && typeof(selImgSize) === 'object'){
                    if(Object.keys(selImgSize).find(key => key === 'ratio')){
                      if(typeof(selImgSize['ratio']) === 'string'){//@TODO - needed here? maybe not
                        if(selImgSize['ratio'] !== ''){
                          if(selImgSize['ratio'].indexOf(':') > -1){
                            ratioNums = selImgSize['ratio'].split(':');
                            aspectRatio = Math.floor(ratioNums[1]) / Math.floor(ratioNums[0]);
                            calculatedSize = {
                              width: Math.floor(selImgSize['width']),
                              height: Math.floor(selImgSize['width'] * aspectRatio)
                            };
                            return calculatedSize;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        console.log('ended ------  sbkCtrl_ImageSize!!!!');
        return selImgSize;
      }

      return 'img return value';
      /*console.log('params.defaultVal:');
      console.log(params['defaultVal'] ? params['defaultVal'] : 'no default');*/
    },

    sbkCtrl_PositionH(params){
      console.log('sbkCtrl_PositionH!!!!');
      /*console.log('params.defaultVal:');
      console.log(params['defaultVal'] ? params['defaultVal'] : 'no default');*/

      return params['default'] ? params['default'] : 'no default';
    },

    sbkCtrl_PositionV(params){
      console.log('sbkCtrl_PositionV!!!!');
      return params['default'] ? params['default'] : 'no default';
    },

    sbkCtrl_TextAlign(params){
      console.log('sbkCtrl_TextAlign!!!!');
      return params['default'] ? params['default'] : 'no default';
    },

    sbkCtrl_Reverse(params){
      console.log('sbkCtrl_Reverse!!!!');
      return params['default'] ? params['default'] : 'no default';
    },

    sbkCtrl_CaptionPosition(params){
      console.log('sbkCtrl_CaptionPosition!!!!');
      return params['default'] ? params['default'] : 'no default';
    },


    sbkCtrl_ShowCaptions(params){
      console.log('sbkCtrl_CaptionPosition!!!!');
      return params['default'] ? params['default'] : 'no default';
    },

    getOptProfileByName(name){
      if( Object.prototype.hasOwnProperty.call(this['sbkCtrlOptProfiles'], name)){
        return this['sbkCtrlOptProfiles'][name];
      }
    }
  }
};
