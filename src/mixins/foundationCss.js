//@TODO - some functions can be nerged with the foundationCss.js file
export default {

  methods: {
    buildColSizeClasses(sizes){
      let colSizeClasses = ' cell ';
      if (Object.keys(sizes).length > 0) {
        for (const [key, value] of Object.entries(sizes)) {
          if(typeof(value) === 'number'){
            if(!isNaN(value)){
              //colSizeClasses += ' column ' + key + '-' + value + ' ';
              colSizeClasses += ' ' + key + '-' + value + ' ';
            }
          }
        }
      }
      return colSizeClasses;
    },

    buildColOffsetClasses(offsets){
      let colOffsetClasses = '';
      if (Object.keys(offsets).length > 0) {
        for (const [key, value] of Object.entries(offsets)) {
          if(typeof(value) === 'number'){
            if(!isNaN(value)){
              let split = key.split('_');
              colOffsetClasses += split[0] + '-' + 'offset' + '-' + value + ' ';
            }
          }
        }
      }
      return colOffsetClasses;
    },

    /*
    buildSbkColOffsetClasses(sizes){
      let sbkColOffsetClasses = ' cell ';
      if (Object.keys(sizes).length > 0) {
        for (const [key, value] of Object.entries(sizes)) {
          if(!isNaN(parseFloat(value))){
            let size = key.split('_offset')[0];
            sbkColOffsetClasses += ' ' + size + '-' + 'offset-' + value + ' ';
          }
        }
      }
      return sbkColOffsetClasses;
    },
*/

    buildImageColClasses(imgSize, imageColSizes) {
      //image_col_sizes
      let imageColClasses = '';

      if(typeof(imgSize) === 'string') {
        if(imgSize !== ''){

          if (!Array.isArray(imageColSizes) && typeof(imageColSizes === 'object')){
            if (Object.keys(imageColSizes).length > 0) {
              if(Object.prototype.hasOwnProperty.call(imageColSizes, imgSize)){
                if (Object.keys(imageColSizes[imgSize]).length > 0) {
                  imageColClasses += this.buildColSizeClasses(imageColSizes[imgSize]);

                  /*for (const [key, value] of Object.entries(imageColSizes[imgSize])) {
                    imageColClasses += this.buildColSizeClasses(value);
                    if (typeof (value) === 'number') {
                      if (!isNaN(value)) {
                        //colSizeClasses += ' column ' + key + '-' + value + ' ';
                        imageColClasses += ' ' + key + '-' + value + ' ';
                      }
                    }
                  }*/
                }
              }
            }
          }
        }
      }
      return imageColClasses;
    },

    buildImageWrapperColClasses(imgSize, imageWrapperColSizes) {
      //image_col_sizes
      let imageWrapperColClasses = '';

      if(typeof(imgSize) === 'string') {
        if(imgSize !== ''){

          if (!Array.isArray(imageWrapperColSizes) && typeof(imageWrapperColSizes === 'object')){
            if (Object.keys(imageWrapperColSizes).length > 0) {
              if(Object.prototype.hasOwnProperty.call(imageWrapperColSizes, imgSize)){
                if (Object.keys(imageWrapperColSizes[imgSize]).length > 0) {
                  imageWrapperColClasses += this.buildColSizeClasses(imageWrapperColSizes[imgSize]);
                }
              }
            }
          }
        }
      }
      return imageWrapperColClasses;
    }
  }
};
