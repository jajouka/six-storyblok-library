export default {
  data() {
    //let self = this;
    //@TODO - couldn't access data from methods
    //@TODO - needs a crop param
    //@TODO - needs a way to override these settings, project level and wrapper level? what about non wrapper content?
    //@TODO - remove col_sizes or use as defaults? wrappers set
    return {
      //image_default: 'square',//@TODO - not used yet

      image_sizes : {
        square: {
          width: 1700,
          ratio: '1:1',
          /*col_sizes: {
            medium: 20,
            large: 20,
          }*/
        },
        small_square: {
          width: 800,
          ratio: '1:1',
          /*col_sizes: {
            medium: 20,
            large: 20,
          }*/
        },
        small_landscape: {
          width: 1200,
          ratio: '1000:670',//67%
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },
        landscape: {
          width: 1700,
          ratio: '5:3',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },
        large_landscape: {
          width: 2000,
          ratio: '5:3',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },
        large_portrait: {
          width: 2000,
          ratio: '7:9',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },
        portrait: {
          width: 1000,
          ratio: '7:9',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },
        small_portrait: {
          width: 500,
          ratio: '7:9',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },
        full_width_landscape: {
          width: 2500,
          ratio: '9:7',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },

        full_width_portrait: {
          width: 2500,
          ratio: '9:12',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        },

        full_width_portrait_xl: {
          width: 3000,
          ratio: '9:12',
          col_sizes: {
            medium: 20,
            large: 20,
          }
        }
      }
    };
  },
  methods: {
    getFormattedBlokImageSizes(){
      let imageSizes = {};
      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_sizes')){
        if(typeof this.blok['image_sizes'] !== 'undefined'){
          if(Array.isArray(this.blok['image_sizes']) && typeof this.blok['image_sizes'] === 'object'){
            if(this.blok['image_sizes'].length > 0){
              this.blok['image_sizes'].forEach( size => {
                if(Object.prototype.hasOwnProperty.call(size, 'image_size_name')){
                  if(typeof size['image_size_name'] !== 'undefined'){
                    if (typeof size['image_size_name'] === 'string'){
                      if(size['image_size_name'].trim() !== ''){
                        if(Object.prototype.hasOwnProperty.call(size, 'image_size_profile_name')){
                          if(typeof size['image_size_profile_name'] !== 'undefined'){
                            if (typeof size['image_size_profile_name'] === 'string'){
                              if(size['image_size_profile_name'].trim() !== ''){
                                imageSizes[size['image_size_name']] = size['image_size_profile_name'];
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              });
            }
          }
        }
      }
      return imageSizes;
    },
    getImageSize(name){
      let imageSize = null;
      if(typeof(name) === 'string'){
        if(name !== ''){
          if(Object.prototype.hasOwnProperty.call(this.image_sizes, name)){
            if(! Array.isArray(this.image_sizes[name]) && typeof(this.image_sizes[name]) === 'object') {
              if (Object.keys(this.image_sizes[name]).length > 0) {
                if (Object.entries(this.image_sizes[name]).length > 0) {
                  if (this.image_sizes[name]) {
                    imageSize = this.image_sizes[name];
                  }
                }
              }
            }
          }
        }
      }
      return imageSize;
    },
    getCalculatedImageSize(name){
      let selImgSize = null;
      let calculatedSize = null;
      let ratioNums = null;
      let aspectRatio = null;

      selImgSize = this.getImageSize(name);

      if(selImgSize){
        if(typeof(selImgSize) !== 'undefined') {
          if(! Array.isArray(selImgSize) && typeof(selImgSize) === 'object') {
            if (Object.keys(selImgSize).find(key => key === 'ratio')) {
              if (typeof (selImgSize['ratio']) === 'string') {
                if (selImgSize['ratio'] !== '') {
                  if (selImgSize['ratio'].indexOf(':') > -1) {
                    ratioNums = selImgSize['ratio'].split(':');
                    aspectRatio = Math.floor(ratioNums[1]) / Math.floor(ratioNums[0]);
                    calculatedSize = {
                      width: Math.floor(selImgSize['width']),
                      height: Math.floor(selImgSize['width'] * aspectRatio)
                    };
                  }
                }
              }
            }
          }
        }
      }
      return calculatedSize;
    }
  }
};
