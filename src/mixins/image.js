export default {
  methods: {
    getAspectRatio(width, height, ifStatement = true) {
      //return this.getOriginalAspectRatio();
      if (ifStatement) {
        return ((Math.round(height) / Math.round(width)) * 100).toFixed(2);
      } else {
        return 0;
      }
    },
  }
};
