export default {
  data(){
    return {
      contentKeyDefault: {
        description: 'subtitle'
      }
    };

  },
  methods: {
    getSbkMetaTitle(){
      let str = this.getValidMetaString(this.story, 'title', true);
      return str ? str : null;
    },
    getSbkMetaDesc(){
      let str = this.getValidMetaString(this.story, 'description', false );
      return str ? str : null;
    },
    getSbkMetaFbTitle(){
      let str = this.getValidMetaString(this.story, 'og_title');
      return str ? str : null;
    },
    getSbkMetaFbDesc(){
      let str = this.getValidMetaString(this.story, 'og_description');
      return str ? str : null;
    },
    getSbkMetaFbImage(){
      let str = this.getValidMetaString(this.story, 'og_image');
      return str ? 'https:' + str : null;
    },
    getSbkMetaTwitterTitle(){
      let str = this.getValidMetaString(this.story, 'twitter_title');
      return str ? str : null;
    },
    getSbkMetaTwitterDesc(){
      let str = this.getValidMetaString(this.story, 'twitter_description');
      return str ? str : null;
    },
    getSbkMetaTwitterImage(){
      let str = this.getValidMetaString(this.story, 'twitter_image');
      return str ? 'https:' + str : null;
    },
    getValidMetaString(story, metaName, title){
      let contentKey = 'content';
      let titleKey = 'title';
      let metaTagsKey = 'meta_tags';
      if(this.story){
        if(Object.prototype.hasOwnProperty.call(story, contentKey)){
          /*if(title){
            if(Object.prototype.hasOwnProperty.call(story[contentKey], titleKey)){
              if(this.story[contentKey][titleKey].trim() !== ''){
                return this.story[contentKey][titleKey].trim();
              }
            }
          }*/
          if(Object.prototype.hasOwnProperty.call(story[contentKey], metaTagsKey)){
            if(Object.prototype.hasOwnProperty.call(story[contentKey][metaTagsKey], metaName)){
              if(this.story[contentKey][metaTagsKey][metaName].trim() !== ''){
                return this.story[contentKey][metaTagsKey][metaName].trim();
              }
            }
          }
        }
      }
      return null;
    },
  }
};
