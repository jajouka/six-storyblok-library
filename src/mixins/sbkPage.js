export default {
  methods: {
    hasSidebar(pageBlok) {
      if (Object.prototype.hasOwnProperty.call(pageBlok, 'sidebar')) {
        if (typeof (pageBlok['sidebar']) !== 'undefined') {
          if (Array.isArray(pageBlok['sidebar']) && typeof (pageBlok['sidebar']) === 'object') {
            if (pageBlok['sidebar'].length > 0) {
              return true;
            }
          }
        }
      }
      return false;
    }
  }
};
