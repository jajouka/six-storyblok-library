import sbkCtrlOptionProfiles from './sbkCtrlOptionProfiles';
import sbkControls from './sbkControls';
export default {

  data() {
    return {
      sbkColPrefix: 'column_',
      sbkSystemFieldNames: [
        'component',
        '_editable',
        '_uid'
      ],
      sbkSixSystemFieldNames: [
        'options_ctype',
        'ctype',
        'project_overrides',

      ],
      tmpHackFieldNotCntList: [//@TODO - HACK - only tmp
        'title',
        'num',
        'test',
      ]
    };
  },
  mixins: [
    sbkCtrlOptionProfiles,
    sbkControls
  ],
  methods: {

    //@TODO - rename to getMergedDataForWrapper(), refactor
    getMergedData(blok, numCols) {
      let data = {};
      //@TODO!!! - needs validating
      //if( this.cntBlokHasValidData(blok)){
      data = this.buildColsDataWithOpts(blok, numCols);
      //}
      return data;
    },

    //@TODO!!! - this needs replacing
    getValidSbkCntKeys(blok){
      let cntKeys = [];
      let sbkControlKeys = this.getSbkCtrlKeys();
      for (let [key, val] of Object.entries(blok)) {
        if (
          this._data.sbkSystemFieldNames.indexOf(key) === -1 &&
          sbkControlKeys.indexOf(key) === -1 &&
            this._data.sbkSixSystemFieldNames.indexOf(key) === -1 &&
          this._data.tmpHackFieldNotCntList.indexOf(key) === -1
        )
        {
          cntKeys.push(key);
        }
      }

      //@TODO
      return cntKeys;
    },

    cntHasBlok(blok){
      return typeof (blok) !== 'undefined';
    },

    cntBlokHasValidData(blok){
      if(!this.cntHasBlok(blok)){
        return false;
      }

      //@TODO - break out of loop as soon as condition is met?

      let valid = false;
      let validSbkCntKeys = this.getValidSbkCntKeys(blok);
      if(validSbkCntKeys.length > 0){
        for (let [key, val] of Object.entries(validSbkCntKeys)) {
          if(Object.prototype.hasOwnProperty.call(blok, val)){
            if(Object.keys(blok[val]).length > 0){
              if(typeof(blok[val]) !== 'undefined'){
                valid = true;
              }
            }
          }
        }
      }
      return valid;
    },

    cntBlokDataStructureIsPresent(blok){
      if(!this.cntHasBlok(blok)){
        return false;
      }
      let valid = false;
      let validSbkCntKeys = this.getValidSbkCntKeys(blok);
      if(validSbkCntKeys.length > 0){
        for (let [key, val] of Object.entries(validSbkCntKeys)) {
          if(Object.prototype.hasOwnProperty.call(blok, val)){
            if(Array.isArray(blok[val])){
              valid = true;
            }
          }
        }
      }
      return valid;
    },

    //ctype dep??
    getProjectOverrides(){
      let projectOverrides = {};
      let optionsCtype = {};
      let mergedOverrides = {};
      if(Object.prototype.hasOwnProperty.call(this.blok, 'project_overrides')){
        if(typeof (this.blok['project_overrides']) !== 'undefined'){
          if(Object.keys(this.blok['project_overrides']).length > 0){
            projectOverrides = this.blok['project_overrides'];
          }
        }
      }
      if(Object.prototype.hasOwnProperty.call(this.blok, 'ctype')){
        if(this.blok.ctype.trim() !== ''){
          if(Object.prototype.hasOwnProperty.call(this.blok, 'options_ctype')) {
            if(typeof( this.blok['options_ctype']) !== 'undefined'){
              if (Object.keys(this.blok['options_ctype']).length > 0) {
                if (Object.prototype.hasOwnProperty.call(this.blok['options_ctype'], this.blok['ctype'])) {
                  if (Object.keys(this.blok['options_ctype'][this.blok['ctype']]).length > 0) {
                    optionsCtype = this.blok['options_ctype'][this.blok['ctype']];
                  }
                }
              }
            }
          }
        }
      }
      mergedOverrides = {...projectOverrides, ...optionsCtype};
      return mergedOverrides;
    },

    addFakeGrpSbkDataWrapper(blok, grpName, fieldName){
      //@TODO - this condition needs fixing, maybe a flat list of groupable cnts
      /*if(Object.prototype.hasOwnProperty.call(blok, grpName) && (2+2===5)){
        return blok;
      }*///
      let modifiedBlok = blok;
      let modifiedCnt = {};
      let items = modifiedBlok[fieldName];

      modifiedCnt = {
        component: grpName,
        items: items,
      };
      if(Object.prototype.hasOwnProperty.call(modifiedBlok, fieldName)){
        if(typeof (modifiedBlok[fieldName]) !== 'undefined'){
          if(Object.keys(modifiedBlok[fieldName]).length > 0){
            delete modifiedBlok[fieldName];
          }
        }
      }
      modifiedBlok[grpName] = modifiedCnt;
      return modifiedBlok;
    },

    getCntToAdd(blok, item){
      let cntToAdd = null;
      if(Object.prototype.hasOwnProperty.call(blok, item)) {

        if (Array.isArray(blok[item]) && typeof (blok[item]) === 'object') {
          if (typeof (blok[item][0]) !== 'undefined') {
            if (Object.keys(blok[item]).length > 0) {//???
              cntToAdd = blok[item][0];
            }
          }
        } else {
          if( ! Array.isArray(blok[item]) && typeof (blok[item]) === 'object'){
            if (Object.keys(blok[item]).length > 0) {
              cntToAdd = blok[item];
            }
          }
          cntToAdd = blok[item];
        }
      }

      return cntToAdd;
    },

    //@TODO - potential problem area when analysing sbk blok data here
    //@TODO - hacky - relates to buttons and links, needs better method of adding wrappers to group sbk blok data
    addColumnWrapperForSingle(blok){
      let self = this;
      let cntsToAdd = [];
      let cntToAdd = {};
      let validCntKeys = this.getValidSbkCntKeys(blok);
      validCntKeys.forEach(function(item) {
        //@TODO - overwrites if more than 1
        cntToAdd = self.getCntToAdd(blok, item);
        if(cntToAdd){
          cntsToAdd.push(cntToAdd);
        }
      });
      return {column_1: cntsToAdd};
    },

    getMergedGridClasses(){
      let gridClasses = '';
      let projectGridClasses = '';
      let projectOverrides = this.getProjectOverrides();
      if(Object.prototype.hasOwnProperty.call(projectOverrides, 'grid_classes')){
        if(projectOverrides['grid_classes'].trim() !== ''){
          projectGridClasses = projectOverrides['grid_classes'];
        }
      }
      if(Object.prototype.hasOwnProperty.call(this, 'overrides')){
        if(Object.prototype.hasOwnProperty.call(this.overrides, 'options')){
          if(Object.prototype.hasOwnProperty.call(this.overrides.options, 'grid_classes')){
            gridClasses = this.overrides.options['grid_classes'];
          }
        }
      }
      return projectGridClasses.trim() !== '' ? projectGridClasses : gridClasses;
    },

    getMergedGridWrapperClasses(){
      let gridWrapperClasses = '';
      let projectGridWrapperClasses = '';
      let projectOverrides = this.getProjectOverrides();
      if(Object.prototype.hasOwnProperty.call(projectOverrides, 'grid_wrapper_classes')){
        if(projectOverrides['grid_wrapper_classes'].trim() !== ''){
          projectGridWrapperClasses = projectOverrides['grid_wrapper_classes'];
        }
      }
      if(Object.prototype.hasOwnProperty.call(this, 'overrides')){
        if(Object.prototype.hasOwnProperty.call(this.overrides, 'options')){
          if(Object.prototype.hasOwnProperty.call(this.overrides.options, 'grid_wrapper_classes')){
            gridWrapperClasses = this.overrides.options['grid_wrapper_classes'];
          }
        }
      }
      return projectGridWrapperClasses.trim() !== '' ? projectGridWrapperClasses : gridWrapperClasses;
    },

    buildColsDataWithOpts(blok, numCols) {
      if(numCols === 1){
        blok = this.addColumnWrapperForSingle(blok);
      }
      let dataCols = {};
      if(typeof(numCols) === 'number'){
        if(numCols > 0) {
          for (let i = 0; i < numCols; i++) {
            let colName = this._data.sbkColPrefix + (i + 1);
            if (Object.prototype.hasOwnProperty.call(blok, colName)) {
              dataCols[colName] = this.buildColDataWithOpts(colName, blok[colName]);
            }
          }
        }
      }

      //@TODO - need proper checks here - need a getter function with checks and a path profile param
      let gridWrapperClasses = this.getMergedGridWrapperClasses();
      let gridClasses = this.getMergedGridClasses();

      return {...dataCols, ...{grid_classes: gridClasses}, ...{grid_wrapper_classes: gridWrapperClasses}};
    },

    buildColDataWithOpts(colName, blokColCnts) {
      //@TODO! - this is a MESS, functional but need rethinking, relates to simplified recursive json iterator solution
      let cntList = [];
      let libColOpts = this.getLibColOpts(colName);
      let projectColOpts = this.getProjectColOpts(colName);
      let merged = {};
      let cnt = null;

      if( Object.entries(blokColCnts).length > 0 && JSON.stringify(Object.keys(blokColCnts[0])) !== '[]'){
        for (let [cntNum, cntValue] of Object.entries(blokColCnts)) {
          if(!Array.isArray(cntValue) && typeof (cntValue) === 'object'){
            if(Object.keys(cntValue).length > 0){
              if(Object.prototype.hasOwnProperty.call(cntValue, 'component')){
                //@TODO - patch fix here, to remove invalid entries for cnts, see addColumnWrapperForSingle(). function
                cnt = this.mergeCntWithOpts(colName, cntNum, cntValue);
                cntList.push(cnt);
              }
            }
          }
        }
      }
      else{
        let mergedColOpts = {...libColOpts, ...projectColOpts};
        let ghostCntOpts = mergedColOpts['components'];
        for(let[name, val] of Object.entries(ghostCntOpts)){
          cntList.push({...{component: name}, ...val});
        }
      }

      //delete libColOpts.components;
      //delete projectColOpts.components;

      merged = {...libColOpts, ...projectColOpts, ...{components: cntList}};
      return merged;
    },

    //@TODO!!! - how to use cntNum here?
    mergeCntWithOpts(colName, cntNum, cntValue){
      let merged = cntValue;
      let projectCntOpt = this.getProjectColCntOpt(colName, cntValue['component']);
      if(projectCntOpt){
        let cntsInColOpts = projectCntOpt[cntValue['component']];
        if(cntsInColOpts){
          let sbkCtrls = this.getSbkCtrlOptsForCnt(colName, cntValue['component'], cntNum);
          merged = {...cntValue, ...cntsInColOpts, ...sbkCtrls};
          //delete merged.sbk_controls;
        }
      }
      return merged;
    },

    getLibColOpts(colName){
      let colOpts = {};
      if(Object.prototype.hasOwnProperty.call(this.overrides.options, colName)){
        colOpts = this.overrides.options[colName];
      }
      //delete colOpts['components'];
      return colOpts;
    },

    getProjectColOpts(colName){
      let projectColOpts = null;
      let projectOverrides = this.getProjectOverrides();
      if( projectOverrides ) {
        if(Object.prototype.hasOwnProperty.call(projectOverrides, colName)){
          projectColOpts = projectOverrides[colName];
        }
      }
      //delete projectColOpts['components'];
      return projectColOpts;
    },

    getProjectColCntOpt(colName, cntName) {
      let projectColOpt = null;
      let projectColOpts = this.getProjectColOpts(colName);
      if(Object.prototype.hasOwnProperty.call(projectColOpts, 'components')){
        if(typeof (projectColOpts['components']) !== 'undefined'){
          if(!Array.isArray(projectColOpts['components']) && typeof (projectColOpts['components']) === 'object'){
            if(Object.prototype.hasOwnProperty.call(projectColOpts['components'], cntName)) {
              if (typeof (projectColOpts['components'][cntName]) !== 'undefined') {
                if (!Array.isArray(projectColOpts['components'][cntName]) && typeof (projectColOpts['components'][cntName] === 'object')) {
                  if (Object.keys(projectColOpts['components'][cntName]).length > 0) {
                    projectColOpt = projectColOpts['components'][cntName];
                  }
                }
              }
            }
          }
        }

      }
      return {[cntName]: projectColOpt};
    },

    getProjectColCntOpts(colName){
      let projectColCntOpts = null;
      let projectOverrides = this.getProjectOverrides();
      if( projectOverrides ) {
        if(Object.prototype.hasOwnProperty.call(projectOverrides, colName)){
          if(Object.prototype.hasOwnProperty.call(projectOverrides[colName], 'components')) {
            if(typeof (projectOverrides[colName]['components']) !== 'undefined'){
              if(!Array.isArray(projectOverrides[colName]['components']) && typeof(projectOverrides[colName]['components'] === 'object')) {
                if(Object.keys(projectOverrides[colName]['components']).length > 0) {
                  projectColCntOpts = projectOverrides[colName]['components'];
                }
              }
            }
          }
        }
      }
      return projectColCntOpts;
    },


    //@TODO!!! - needed still?
    buildGhostCntWithOpts(cnt){
      let builtGhostCnt = cnt;
      let opts = {
        layout: 'dcef735a-962e-4511-af67-e0c7acddfc75',
        order_by: 'first_published_date_asc',
        starts_with: 'sustainability/materials-we-use',
        exclude_current_page: true,
      };

      return { ...builtGhostCnt, ...opts};
      /*if(){

      }*/
    },
  }
};
