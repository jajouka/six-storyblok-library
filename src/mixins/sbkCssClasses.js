import buildColSizeClasses from './foundationCss';

export default {
  data(){
    //@TODO - move to foundation js file
    return{
      classNames:{
        column: ' cell ',
        grid: ' grid-x ',
      }
    };
  },
  mixins: [
    buildColSizeClasses
  ],
  methods: {
    imageSize(){
      let imageSize = '';
      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_size')) {
        if (typeof (this.blok['image_size']) === 'string') {
          if (this.blok['image_size'] !== '') {
            imageSize = this.blok['image_size'];
          }
        }
      }
      return imageSize;
    },
    positionHClasses(){
      if (Object.prototype.hasOwnProperty.call(this.blok, 'image_position_sizes')) {
        if (typeof (this.blok.image_position_sizes) === 'object') {
          if(this.imageSize() !== ''){
            if(Object.prototype.hasOwnProperty.call(this.blok.image_position_sizes, this.imageSize())){
              if(typeof (this.blok.image_position_sizes[this.imageSize()]) === 'string'){
                if(this.blok.image_position_sizes[this.imageSize()] !== ''){
                  return ` align-${this.blok.image_position_sizes[this.imageSize()]} `;
                }
              }
            }
          }
        }
      }
      if (Object.prototype.hasOwnProperty.call(this.blok, 'position_h')) {
        if (typeof (this.blok.position_h) === 'string') {
          return ` align-${this.blok.position_h} `;//@TODO - needs valid list?
        }
      }

      //TODO - needs to work for image sizes
      return '';
    },

    /*imageSizeWrapperColClasses(){
      let builtColClasses = ' column ';
      if(this.imageSize()){//@TODO - checks needed
        builtColClasses += this.buildImageColClasses(this.imageSize(), this.imageWrapperColSizes);
      }
      return builtColClasses;
    },*/

    textAlignClasses(){
      let defaultTextAlign = 'left';
      let textAlign = '';
      if (Object.prototype.hasOwnProperty.call(this.blok, 'text_align')) {
        if (typeof (this.blok.text_align) === 'string') {
          textAlign = this.blok.text_align ? this.blok.text_align : defaultTextAlign;
          return ` u-align--${textAlign} `;
        }
      }
      return '';
    },

    imageSizeColClasses(){
      //@TODO - should not be here, needs to return be able to empty string
      let builtColClasses = this._data.classNames.column;
      if(this.imageSize()){//@TODO - checks needed
        builtColClasses += this.buildImageColClasses(this.imageSize(), this.imageColSizes);
      }
      return builtColClasses;
    },

    hasImagePositionControl(imgSize){
      if(!imgSize) {
        return false;
      }
      let imagePositionControl = false;
      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_position_control')) {
        if(Object.keys(this.blok['image_position_control']).length > 0){
          if(Object.prototype.hasOwnProperty.call(this.blok['image_position_control'], imgSize)) {
            if (typeof (this.blok['image_position_control'][imgSize]) === 'boolean') {
              if (this.blok['image_position_control'][imgSize]) {
                imagePositionControl = true;
              }
            }
          }
        }
      }
      return imagePositionControl;
    },

    hasImagePositionSizes(imgSize){
      if(!imgSize) {
        return false;
      }
      let imagePositionSize = false;
      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_position_sizes')) {
        if(Object.keys(this.blok['image_position_sizes']).length > 0){
          if(Object.prototype.hasOwnProperty.call(this.blok['image_position_sizes'], imgSize)) {
            if (typeof (this.blok['image_position_sizes'][imgSize]) === 'string') {
              if (this.blok['image_position_sizes'][imgSize].trim() !== '') {
                imagePositionSize = true;
              }
            }
          }
        }
      }
      return imagePositionSize;
    },

    imageClassesForSize(imgSize){
      if(!imgSize) {
        return '';
      }
      let imageClassesForSize = false;
      if(Object.prototype.hasOwnProperty.call(this.blok, 'image_classes')) {
        if(Object.keys(this.blok['image_classes']).length > 0){
          if(Object.prototype.hasOwnProperty.call(this.blok['image_classes'], imgSize)) {
            if (typeof (this.blok['image_classes'][imgSize]) === 'string') {
              if( this.blok['image_classes'][imgSize].length > 0){
                imageClassesForSize = this.blok['image_classes'][imgSize];
              }
            }
          }
        }
      }
      return imageClassesForSize;
    },

    imageSizeNameClasses(){
      let classes = '';
      const prefix = 'image-size-';
      if(this.imageSize()){
        classes = prefix + this.imageSize();
      }
      return classes;
    },

    gridClasses(){
      let classes = this._data.classNames.grid;
      if(Object.prototype.hasOwnProperty.call(this.blok, 'grid_classes')){
        if(typeof (this.blok['grid_classes']) === 'string'){
          classes += this.blok['grid_classes'];
        }
      }
      return classes;
    },

    gridWrapperClasses(){
      let classes = '';
      if(Object.prototype.hasOwnProperty.call(this.blok, 'grid_wrapper_classes')){
        if(typeof (this.blok['grid_wrapper_classes']) === 'string'){
          classes += this.blok['grid_wrapper_classes'];
        }
      }
      return classes;
    },

    gutterClasses() {
      //@TODO - name changed needed, for e.g. this.blok.col_classes
      let gutterClasses = ''; //@TODO - better checks needed, put ' cell ' class here?
      if (Object.prototype.hasOwnProperty.call(this.blok, 'left_gutter')) {
        if(this.blok['left_gutter']){
          gutterClasses += ' left-gutter ';
        }
      }
      if (Object.prototype.hasOwnProperty.call(this.blok, 'right_gutter')) {
        if(this.blok['right_gutter']){
          gutterClasses += ' right-gutter ';
        }
      }
      return gutterClasses;
    },

    widthClasses(){
      //@TODO - change this to modify col sizes instead
      let widthClasses = '';
      if (Object.prototype.hasOwnProperty.call(this.blok, 'width')) {
        if (typeof (this.blok['width']) === 'string') {
          widthClasses += ' width-' + this.blok['width'];
        }
      }
      return widthClasses;
    },

    blockStyleClasses(){
      let blockStyleClasses = '';
      if (Object.prototype.hasOwnProperty.call(this.blok, 'block_style')) {
        if (typeof (this.blok['block_style']) === 'string') {
          blockStyleClasses += ' block_style-' + this.blok['block_style'];
        }
      }
      return blockStyleClasses;
    },

    //@TODO - Not working
    removeSpaceClasses() {
      let noTopSpaceClasses = ' ';
      let noBottomSpaceClasses = ' ';
      if (Object.prototype.hasOwnProperty.call(this.blok, 'remove_top_space')) {
        if (this.blok['remove_top_space']) {
          noTopSpaceClasses += ' remove-top-space ';
        }
      }
      if (Object.prototype.hasOwnProperty.call(this.blok, 'remove_bottom_space')) {
        if (this.blok['remove_bottom_space']) {
          noBottomSpaceClasses += ' remove-bottom-space ';
        }
      }

      return noTopSpaceClasses + noBottomSpaceClasses;
    },

    blokClasses() {
      //@TODO - name changed needed, for e.g. this.blok.col_classes
      let blokClasses = ''; //@TODO - better checks needed, put ' cell ' class here?
      if (Object.prototype.hasOwnProperty.call(this.blok, 'classes')) {
        blokClasses = this.blok.classes;
      }
      return blokClasses;
    },

    colOffsetClasses() {
      let classes = '';
      let useOffset = false;
      if(Object.prototype.hasOwnProperty.call(this.blok, 'position_h')) {
        if (typeof (this.blok['position_h']) === 'string') {
          if(this.blok['position_h'] === '' || this.blok['position_h'] === 'offset'){
            useOffset = true;
          }
        }
      }

      if(useOffset || ! Object.prototype.hasOwnProperty.call(this.blok, 'position_h')){
        if(Object.prototype.hasOwnProperty.call(this.blok, 'col_offsets')){
          if(typeof (this.blok['col_offsets']) === 'object'){
            classes += this.buildColOffsetClasses(this.blok['col_offsets']);
          }
        }
      }

      return classes;
    },

    colClasses(){
      let classes = this._data.classNames.column;
      if(Object.prototype.hasOwnProperty.call(this.blok, 'col_classes')){
        if(typeof (this.blok['col_classes']) === 'string'){
          classes += this.blok['col_classes'];
        }
      }
      if(Object.prototype.hasOwnProperty.call(this.blok, 'col_sizes')){
        if(typeof (this.blok['col_sizes']) === 'object'){
          classes += this.buildColSizeClasses(this.blok['col_sizes']);
        }
      }
      return classes;
    },

    stickyModifierClass(name){
      let classes = '';
      if( this.blok.sticky ){
        classes = `${name}--sticky`;
      }
      return classes;
    },


  }
};
