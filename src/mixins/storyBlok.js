//@TODO - change filename, split code?
//@TODO - harden security of mixins
//@TODO - put story bridge & visual editor in its own file
//@TODO - put API request functions into api file
//@TODO - decide whether to use this a dep, if mixin restrictions are discovered,
//          it may be better to make methods self contained for easier migration
//@TODO - better use of error callbacks needed
import getSbkMetaDesc from './sbkMeta';
import getSbkMetaFbImage from './sbkMeta';
import getSbkMetaFbTitle from './sbkMeta';
import getSbkMetaFbDesc from './sbkMeta';
import getSbkMetaTwitterImage from './sbkMeta';
import getSbkMetaTwitterTitle from './sbkMeta';
import getSbkMetaTwitterDesc from './sbkMeta';
import { EventBus } from '@/plugins/event-bus';

import 'object-path';
let objectPath = require('object-path');

/*const StoryBlokClient = require('storyblok-js-client');

let StoryBlokDeliveryClient = new StoryBlokClient({
  accessToken: 'rCxO2Y9nZMwM9owg0dQtFAtt', ///@TODO!!!!! - move to env - change key
  cache: {
    clear: 'auto',
    type: 'memory'
  }
});*/

export default {
  mixins: [
    getSbkMetaDesc,
    getSbkMetaFbTitle,
    getSbkMetaFbDesc,
    getSbkMetaFbImage,
    getSbkMetaTwitterDesc,
    getSbkMetaTwitterImage,
    getSbkMetaTwitterTitle
  ],
  methods: {
    getCntUid () {
      return this.$options.name + '__' + this._uid;
    },
    errorCallback(){
      console.log('error found');
      return 'error';
    },
    getCleanOptions(options){
      if(typeof options !== 'object') {
        console.log('ERROR : options parameter is not an object');
        return {};
      }
      let newOptions = {};
      for(let k in options) {
        if (Object.prototype.hasOwnProperty.call(options, k)) {
          newOptions[k] = options[k];
        }
      }
      return newOptions;
    },

    formatLayouts(data) {
      if(data.length > 0){
        let layouts = [];
        for(let x = 0; x < data.length; x++){
          let layout = {};
          layout['id'] = data[x].uuid;
          layout['component_name'] = data[x].content.component_name;
          layout['image'] = data[x].content.image;
          layout['label'] = data[x].content.label;
          layout['value'] = data[x].content.value;
          layouts.push(layout);
        }
        return layouts;
      }
      return [];
    },

    getLayoutNumberWithId(layouts, id){
      //@TODO - get from vuex, cascading data needed
      if(layouts.length > 0){
        for(let x = 0; x < layouts.length; x++){
          if(layouts[x].uuid === id){
            return layouts[x].content.value;
          }
        }
      }
    },

    getLayoutIdWithNumber(layouts, number){
      //@TODO - type checking needed
      if(number > 0){
        return layouts[number].content.value;
      }
    },

    setLayout(layout){
      let self = this;
      layout.then(function(res){
        self.layout = res.content;
      });
    },

    isSbUUID ( uuid ) {
      let s = '' + uuid;
      s = s.match('{?[0-9a-f]{8}-?[0-9a-f]{4}-?[1-5][0-9a-f]{3}-?[89ab][0-9a-f]{3}-?[0-9a-f]{12}}?');
      return s !== null;
    },

    getSbkPageMeta(story, domain, desc){
      return [
        {
          hid: 'description',
          name: 'description',
          content:   this.getSbkMetaDesc() || desc || '',
        },
        {
          hid: 'og:type',
          property: 'og:type',
          content: 'website'
        },
        {
          hid: 'og:title',
          property: 'og:title',
          content: this.getSbkMetaFbTitle() || this.getSbkMetaTitle() || story.content.title || ''
        },
        {
          hid: 'og:description',
          property: 'og:description',
          content: this.getSbkMetaFbDesc() || this.getSbkMetaDesc() || desc || ''
        },
        {
          hid: 'og:url',
          property: 'og:url',
          content: domain + this.$route.fullPath
        },
        {
          hid: 'og:image',
          property: 'og:image',
          content: this.getSbkMetaFbImage() || ''
        },
        {
          hid: 'twitter:title',
          name: 'twitter:title',
          content: this.getSbkMetaTwitterTitle() || this.getSbkMetaTitle() || story.content.title || ''
        },
        {
          hid: 'twitter:description',
          name: 'twitter:description',
          content:  this.getSbkMetaTwitterDesc() || this.getSbkMetaDesc() || desc || ''
        },
        {
          hid: 'twitter:image',
          name: 'twitter:image',
          content: this.getSbkMetaTwitterImage() || ''
        },
      ];
    },

    setStorePageData(){
      this.$store.dispatch('loadStory', this.story );
    },

    setStoreStoriesData(){
      this.$store.dispatch('loadStories', this.stories );
    },

    getStoreStoryContentData(content, key){
      let val = content[key];
      return val;
    },

    /*setAndGetEditModeClient(){
      // @TODO - must merge with  setAndGetEditMode()
      let context = this.$nuxt.context;
      let editMode = false;
      if (context.query._storyblok || context.isDev || (typeof window !== 'undefined' && window.localStorage.getItem('_storyblok_draft_mode'))) {
        if (typeof window !== 'undefined') {
          window.localStorage.setItem('_storyblok_draft_mode', '1');
          if (window.location === window.parent.location) {
            window.localStorage.removeItem('_storyblok_draft_mode');
          }
        }
        editMode = true;
      }
      return editMode;
    },*/

    /*setAndGetEditMode(context){
      //@TODO - needs checking closely in all scenarios

      //console.log('setAndGetEditMode()');

      /!*console.log('context:');
      console.log(context);*!/

      let editMode = false;
      if (context.query._storyblok || context.isDev || (typeof window !== 'undefined' && window.localStorage.getItem('_storyblok_draft_mode'))) {
        if (typeof window !== 'undefined') {
          window.localStorage.setItem('_storyblok_draft_mode', '1');
          if (window.location === window.parent.location) {
            window.localStorage.removeItem('_storyblok_draft_mode');
          }
        }
        editMode = true;
      }

      //console.log('end!');

      return editMode;
    },*/

    setStoryBridgeEventHandlers() {//@TODO - inefficient code - needs to change only updated data?
      this.$storybridge.on(['input', 'published', 'change'], (event) => {
        if (event.action === 'input') {
          if (event.story.id === this.story.id) {
            let uuids = this.buildRelFieldUuids(event, this.relFields);
            let payload = {
              api: this.$storyapi,
              by_uuids: uuids
            };
            this.$store.dispatch('sbk/sbkStore/loadStoryBridgeData', payload ).then(() => {
              if(this.updateStoryBridgeDataCallback instanceof Function){
                this.updateStoryBridgeDataCallback(event, this);
              }
              else{
                console.log('error: updateStoryBridgeDataCallback is not a function');
              }
            });
          }
        } else if (!event.slugChanged) {
          window.location.reload();
        }
      });
    },

    updateStoryBridgeDataCallback(event, obj){//@TODO - need obj?
      obj.story.content = event.story.content;
      let relFieldData = this.getUpdatedRelFieldData(obj, event);//@TODO - need obj?
      if (Object.keys(relFieldData).length > 0) {
        for (const key in relFieldData) {
          if (Object.prototype.hasOwnProperty.call(relFieldData, key)) {//@TODO - use es6 syntax for Object call
            // if(Object.prototype.hasOwnProperty.call(obj, key)) {
            this.updateData(obj.story.content, relFieldData[key], key);
            // }
          }
        }
      }
      EventBus.$emit('sbkEditorUpdated', true);
    },

    updateData(obj, data, fieldKey) {//@TODO - need obj? rename method
      obj[fieldKey] = data;
    },

    getUpdatedRelFieldData (obj, data) {
      let filteredData = [];
      let tmpObj = {};
      if(Object.prototype.hasOwnProperty.call(obj, 'relFields')){
        if(typeof obj.relFields === 'object' ) {
          if (Object.keys(obj.relFields).length > 0) {
            for (let field in obj.relFields) {
              if(Object.prototype.hasOwnProperty.call(obj.relFields, field)){
                if(typeof field === 'string' ) {
                  if (field.length > 0) {
                    tmpObj = this.getFilteredRelFieldData(field, obj, data);
                    filteredData[field] = tmpObj;
                  }
                }
              }
            }
          }
        }
      }
      return filteredData;
    },

    //@TODO - move to projext?
    getFilteredRelFieldData (field, obj, data) {
      let objRefStr = `story.content.${field}`;
      let newVal = objectPath.get(data, objRefStr);
      let filteredData, filteredDataItem;

      if(!newVal) { return null; }

      if(Array.isArray(newVal)){
        filteredData = [];
        newVal.forEach(function(key){
          filteredDataItem = obj.$store.state.sbk.sbkStore.data.stories.filter(story => {
            return story.uuid.includes(key);
          })[0];

          filteredData.push(filteredDataItem);
        });

      } else {
        filteredData = obj.$store.state.sbk.sbkStore.data.stories.filter(story => {
          return story.uuid.includes(newVal);
        })[0];
      }

      return filteredData ? filteredData : null;
    },

    buildRelFieldUuids(event, relFields) {
      let fieldArr = [];
      if(typeof relFields === 'object' ) {
        if (Object.keys(relFields).length > 0) {
          for (let field in relFields) {
            if(Object.prototype.hasOwnProperty.call(relFields, field)){
              let objRefStr = `story.content.${field}`;
              let value = objectPath.get(event, objRefStr);
              if(typeof(value) !== 'undefined') {
                if(Object.prototype.hasOwnProperty.call(value, 'length')){
                  if (value.length > 0) {
                    if (value instanceof Array) {
                      fieldArr = fieldArr.concat(value);
                    } else {
                      fieldArr.push(value);
                    }
                  }
                }
              }
            }
          }
        }
      }
      return fieldArr;
    },

    buildRelationFieldStrFromFields(relFields) {
      let str = '';
      for (const field in relFields){
        str += field + ',';
      }
      return str.substring(0, str.length -1);
    },

    isWrapperCnt(name){
      //@TODO - proper checks needed on name, sbk can present blank in some situations
      return name.indexOf('wrapper_') > -1;
    },

    isVimeoCnt(name){
      return name === 'plugin_vimeo_video';
    },

    getVimeoComponentName(name) {
      if(!name){
        return '';
      }
      return 'c-Vimeo';
    },

    cName(name, ctype){
      //name = 'wrapper_intro_full';
      if(!name || name.trim() === ''){
        return '';
      }
      if(this.isWrapperCnt(name)){
        name = this.getWrapperComponentName(name, ctype);
      }
      else if(this.isVimeoCnt(name)){
        name = this.getVimeoComponentName(name);
      }
      else{
        name = this.getComponentName(name);
      }

      return name;
      //return this.checkComponentExistsByName(this.getChildComponentName(name))
      // ? this.getChildComponentName(name)
      // : this.getComponentName(name);
    },

    getWrapperComponentName(name, ctype) {
      if(!name){
        console.log('getWrapperComponentName - name doesnt exist');
        return '';
      }
      let cntName = '';
      let ctypeAffix = '';
      if(ctype){
        ctypeAffix = '--' + ctype;
      }
      cntName =  `c-wrp-${name.toLowerCase().replace('wrapper_', '').replace(/_/g, '-').replace('-copy', '')}${ctypeAffix}`;
      return cntName;
    },

    getComponentName(name) {
      if(!name){
        console.log('getComponentName - name doesnt exist');
        return '';
      }
      return `c-${name.toLowerCase().replace(/_/g, '-')}`;
    },

    getChildComponentName(name){
      return name.replace(/_/g, '-') + '-child';
    },

    checkComponentExistsByName(name) {
      let exists = true;//typeof (this.$options.components[name] ) !== 'undefined';
      return exists;
    },

    buildAuthors(storyContent, authorFields){
      let authors = [];
      let i = 0;
      if(authorFields) {
        if (authorFields['length'] !== 'undefined') {
          let len = authorFields.length;
          if (len > 0) {
            if (!Array.isArray(storyContent) && typeof storyContent === 'object') {
              if (Object.keys(storyContent).length > 0 ) {
                while (i < len) {
                  let label = authorFields[i].replace('_', ' ');
                  //console.log(label);
                  if(label.toLowerCase() === 'photography by'){
                    label = 'Photographed By';
                  }
                  if (storyContent[authorFields[i]]) {
                    authors.push({...storyContent[authorFields[i]], ...{label: label}});
                  }
                  i++;
                }

                return this.formatAuthors(authors);
              }
            }
          }
        }
      }
      return [];
    },

    formatAuthors(authors){
      let builtAuthors = [];
      if(typeof (authors) !== 'undefined'){
        for(let [idx, author] of Object.entries(authors)){

          //@TODO - needs proper checks
          let builtAuthor = {
            firstname: author.content.firstname,
            surname: author.content.surname,
            label: author.label,
            description: author.content.description,
          };
          builtAuthors.push(builtAuthor);
        }
      }
      return builtAuthors;
    },

    figLabel() {
      return this.blok.label !== '' ? this.blok.label : '';
    },

    /*isWrapperCnt(blok){
      let isWrapper = false;
      if(Object.keys(blok).length > 0){
        if(Object.prototype.hasOwnProperty.call(blok, 'component')){
          if(typeof (blok['component']) === 'string'){
            if(blok['component'].indexOf('wrapper_') > -1){
              isWrapper = true;
            }
          }
        }
      }
      return isWrapper;
    }*/

    ///@TODO needs renaming, specific to each layout
    getPostBoxFields(story, mediaOverride){
      let media = mediaOverride || story.content.feature_image || story.content.cover_image;

      let headerBgColor = '#ffffff';
      if(Object.prototype.hasOwnProperty.call(story.content, 'header_background_color')){
        if(typeof story.content.header_background_color !== 'undefined'){
          if(Object.prototype.hasOwnProperty.call(story.content.header_background_color, 'color')) {
            if(typeof story.content.header_background_color.color !== 'undefined'){
              if(story.content.header_background_color.color.trim() !== ''){
                headerBgColor = story.content.header_background_color.color;
              }
            }
          }
        }
      }

      if(typeof (story) !== 'undefined'){
        if(Object.keys(story).length > 0 ){
          return {
            title: this.blok.title || story.content.title || story.name,
            date: story.published_at ? story.published_at : '',
            date_first_published: story.first_published_at ? story.first_published_at : '',
            category: story.content.category ? story.content.category : '',
            subtitle: this.blok.subtitle || story.content.subtitle,
            image: media,
            link: story.full_slug ? `/${story.full_slug}` : '',
            use_video_in_lists: story.content.use_video_in_lists || false,
            header_layout: story.content.header_layout || 'full',
            media_overlay: story.content.media_overlay || false,
            header_background_color: headerBgColor,
            magazine_type : story.content.magazine_type || false,
            file_link : story.content.file_link || false,
            magazine_link : story.content.link || false,
            release_date : story.content.release_date || false
          };
        }
      }
      return {};
    },

    async getCurrentStoryId(){
      //@TODO!!! - is this the correct way to get the id?
      let currentId = null;
      if(!isNaN(this.$nuxt.$route.query['_storyblok'])){
        currentId = this.$nuxt.$route.query['_storyblok'];
      }
      else{
        let story = await this.getStory(this.$nuxt.$route.fullPath);
        currentId = story['id'];
      }
      return currentId;
    },

    mediaType(){
      let mediaType = 'image';
      if(Object.prototype.hasOwnProperty.call(this.blok, 'fields')){
        if(Object.prototype.hasOwnProperty.call(this.blok['fields'], 'image')){
          if(Array.isArray(this.blok['fields']['image']) && typeof (this.blok['fields']['image'])){
            if(this.blok['fields']['image'].length > 0){


              ////edit here


              if(this.blok['fields']['image'][0]['component'] === 'plugin_vimeo_video'){

                if(typeof(this.blok['use_video_in_lists']) !== 'undefined'){
                  if(this.blok['use_video_in_lists']){
                    mediaType = 'video';
                  }
                }
              }


            }
          }
        }
      }
      return mediaType;
    }
  }
};
