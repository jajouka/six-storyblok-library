import { storiesOf } from '@storybook/vue';
import Figure from './Figure';

storiesOf('Figure', module)
  .add('Default', () => ({
    components: {
      Figure
    },
    data() {
      return {
        blok: {
          link_url: '#',
          link_text: 'Sample Text'
        }
      };
    },
    template: '<Figure :blok="blok" />'
  }));
