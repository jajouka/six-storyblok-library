import { storiesOf } from '@storybook/vue';
import Link from './Link';

storiesOf('Link', module)
  .add('Default', () => ({
    components: {
      Link
    },
    data() {
      return {
        blok: {
          link_url: '#',
          link_text: 'Sample Text'
        }
      };
    },
    template: '<Link :blok="blok" />'
  }));
