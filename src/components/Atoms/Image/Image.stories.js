import { storiesOf } from '@storybook/vue';
import Image from './Image';

storiesOf('Image', module)
  .add('Default', () => ({
    components: {
      Image
    },
    data() {
      return {
        blok: {
          link_url: '#',
          link_text: 'Sample Text'
        }
      };
    },
    template: '<Image :blok="blok" />'
  }));
