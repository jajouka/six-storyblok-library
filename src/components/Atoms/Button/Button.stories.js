import { storiesOf } from '@storybook/vue';
import Button from './Button';

storiesOf('Button', module)
  .add('Default', () => ({
    components: {
      Link
    },
    data() {
      return {
        blok: {
          url: '#',
          text: 'Sample Text'
        }
      };
    },
    template: '<Button :blok="blok" />'
  }));
